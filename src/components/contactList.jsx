import React, { Component } from "react";
// import * as opertaions from './operations';
class Contact extends Component {
  state = {
    contact: this.props.data,
    delete: this.props.onDelete,
    edit: this.props.onEdit
  };
  render() {
    return (
      <tr key={this.state.contact.id} id={this.state.contact.id}>
        <td className="align-middle"> {this.state.contact.id} </td>
        <td className="align-middle">
          <img
            style={{ borderRadius: 50, height: 50 }}
            src={this.state.contact.avatar_url}
            alt="no-img"
          />
        </td>
        <td className="align-middle">
          <span>{this.state.contact.first_name} </span>
          <span> {this.state.contact.last_name}</span>
        </td>
        <td className="align-middle"> {this.state.contact.phone}</td>
        <td className="align-middle">
          <button
            type="button"
            data-toggle="modal"
            data-target="#editModal"
            onClick={this.state.edit}
            id={this.state.contact.id}
            className="btn btn-secondary"
          >
            EDIT
          </button>
          &nbsp;&nbsp;
          <button
            type="button"
            id={this.state.contact.id}
            onClick={this.state.delete}
            className="btn btn-danger"
          >
            DELETE
          </button>
        </td>
      </tr>
    );
  }
}

export default Contact;
