import React, { Component, Fragment } from "react";
class EditContact extends Component {
  render() {
    return (
      <Fragment>
        <div
          className="modal fade"
          id="editModal"
          role="dialog"
          tabIndex="1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  EDIT CONTACT
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                {Object.values(this.props.data).map(e => (
                  <form key={e.id} id={e.id} onSubmit={this.props.submit}>
                    <div className="form-row">
                      <div className="form-group col-md-6">
                        <label htmlFor="inputEmail4"> FIRST NAME</label>
                        <input
                          type="text"
                          defaultValue={e.first_name}
                          className="form-control"
                          id="first_name"
                          required
                        />
                      </div>
                      <div className="form-group col-md-6">
                        <label htmlFor="inputLastName">LAST NAME</label>
                        <input
                          type="text"
                          defaultValue={e.last_name}
                          className="form-control"
                          id="last_name"
                        />
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-12">
                        <label htmlFor="inputAvathar">
                         AVATAR URL
                        </label>
                        <input
                          type="text"
                          defaultValue={e.avatar_url}
                          className="form-control"
                          id="avatar_url"
                          placeholder="AVATAR URL"
                        />
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-12">
                        <label htmlFor="inputPhoneNumber">CONTACT NUMBER</label>
                        <input
                          type="text"
                          defaultValue={e.phone}
                          className="form-control"
                          id="phone"
                          required
                        />
                      </div>
                      <div className="form-group col-md-12">
                        <label htmlFor="inputEmailAdress">EMAIL</label>
                        <input
                          type="text"
                          defaultValue={e.email}
                          className="form-control"
                          id="email"
                          required
                        />
                      </div>
                    </div>
                    <div className="modal-footer">
                      <button
                        type="button"
                        className="btn btn-secondary"
                        data-dismiss="modal"
                      >
                        Close
                      </button>
                      <button type="submit" className="btn btn-primary">
                        SUBMIT
                      </button>
                    </div>
                  </form>
                ))}
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default EditContact;
