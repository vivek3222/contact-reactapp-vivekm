import React, { Component, Fragment } from "react";
class AddItem extends Component {
  state = {};
  render() {
    return (
      <Fragment>
        <div
          className="modal fade"
          id="CreateModal"
          role="dialog"
          tabIndex="1"
          aria-labelledby="CreateModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="CreateModalLabel">
                  ADD NEW CONTACT
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form onSubmit={this.props.submit}>
                  <div className="form-row">
                    <div className="form-group col-md-6">
                      <input
                        type="text"
                        className="form-control"
                        id="first_name"
                        placeholder="FIRST NAME"
                        required
                      />
                    </div>
                    <div className="form-group col-md-6">
                      <input
                        type="text"
                        className="form-control"
                        id="last_name"
                        placeholder="LAST NAME"
                      />
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="form-group col-md-12">
                      <input
                        type="text"
                        defaultValue="https://robohash.org/iustoautemfacere.png?size=100x100&set=set1"
                        className="form-control"
                        id="avatar_url"
                      />
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="form-group col-md-12">
                      <input
                        type="text"
                        className="form-control"
                        id="phone"
                        placeholder="CONTACT NUMBER"
                        required
                      />
                    </div>
                    <div className="form-group col-md-12">
                      <input
                        type="email"
                        className="form-control"
                        id="email"
                        placeholder="EMAIL"
                        required
                      />
                    </div>
                  </div>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-secondary"
                      data-dismiss="modal"
                    >
                      Close
                    </button>
                    <button type="submit" className="btn btn-primary">
                      SUBMIT
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default AddItem;
