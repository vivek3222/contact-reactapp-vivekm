import React, { Component, Fragment } from "react";
class Search extends Component {
  render() {
    return (
      <Fragment>
        <div className="input-group m-3">
          <div className="input-group-prepend">
            <span className="input-group-text" id="basic-addon1">
              SEARCH
            </span>
          </div>
          <input
            type="text"
            className="form-control"
            onChange={this.props.search}
            placeholder="SEARCH CONTACTS"
            aria-label="Name"
            aria-describedby="basic-addon1"
          />
          <div className="input-group-append">
            <button
              className="btn btn-outline-primary"
              data-toggle="modal"
              data-target="#CreateModal"
              type="button"
            >
              Create New Contact
            </button>
          </div>
        </div>
      </Fragment>
    );
  }
}
export default Search;
