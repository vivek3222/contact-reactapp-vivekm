import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.css";
import "./App.css";
import data from "./jsonData/contacts.json";
import ContactDetails from "./components/contactList";
import ContactEdit from "./components/contactEdit";
import AddItem from "./components/addItems";
import Search from "./components/search";
localStorage.setItem("data", JSON.stringify(data));

class App extends Component {
  state = {
    contacts: JSON.parse(localStorage.getItem("data")),
    searchContact: "",
    editContact: {}
  };

  handlerSearch = element => {
    this.setState({ searchContact: element.target.value });
  };

  handlerCreate = contact => {
    contact.preventDefault();
    const contacts = [...this.state.contacts];
    const newContact = {
      id: contacts.length + 1,
      first_name: contact.target.first_name.value,
      last_name: contact.target.last_name.value,
      email: contact.target.email.value,
      avatar_url: contact.target.avatar_url.value,
      phone: contact.target.phone.value
    };
    contacts.push(newContact);
    this.setState({ contacts: contacts });
  };

  handlerEdit = id => {
    const editData = Object.values(this.state.contacts).filter(
      event => event.id === parseInt(id.target.id)
    );
    this.setState({ editContact: editData });
  };

  handlerUpdate = event => {
    event.preventDefault();
    const contact = [...this.state.contacts];
    const updatedContact = contact.map(contact => {
      if (contact.id === parseInt(event.target.id)) {
        contact.first_name = event.target.first_name.value;
        contact.last_name = event.target.last_name.value;
        contact.email = event.target.email.value;
        contact.avatar_url = event.target.avatar_url.value;
        contact.phone = event.target.phone.value;
      }
      return contact;
    });
    this.setState({ contacts: updatedContact });
  };

  deleteHandler = element => {
    const filteredData = [...this.state.contacts].filter(
      e => e.id !== parseInt(element.target.id)
    );
    this.setState({ contacts: filteredData });
  };

  render() {
    return (
      <div className="container">
        <h1>CONTACTS</h1>
        <Search search={this.handlerSearch} />
        <div className="row">
          <table className="table table-hover table-dark table-borderless">
            <thead>
              <tr>
                <th>S.No</th>
                <th>Profile Picture</th>
                <th>Full Name</th>
                <th>Phone Number</th>
                <th>Operations</th>
              </tr>
            </thead>
            <tbody>
              {Object.values(this.state.contacts)
                .filter(
                  e =>
                    e.first_name
                      .toLowerCase()
                      .includes(this.state.searchContact.toLowerCase()) ||
                    e.last_name
                      .toLowerCase()
                      .includes(this.state.searchContact.toLowerCase())
                )
                .map(contact => (
                  <ContactDetails
                    key={contact.id}
                    onEdit={this.handlerEdit}
                    onDelete={this.deleteHandler}
                    data={contact}
                  />
                ))}
            </tbody>
          </table>
        </div>
        <ContactEdit
          submit={this.handlerUpdate}
          data={this.state.editContact}
        />
        <AddItem submit={this.handlerCreate} />
      </div>
    );
  }
}
export default App;
